from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from .models import Task, TaskList
from .forms import TaskForm, TaskFormDelete, CreateTaskForm, TaskListForm
from django .contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
from datetime import date
from django.contrib.auth.models import User
from django.core import serializers


#TODO: poprawić przesylanie requestów w forms 16.01 
def home_view(request):
    tasks = Task.objects.all()
    task_list_dict = {}
    tasks_of_specific_tasklist = []
    # print(request.user)
    if not request.user.is_anonymous:
        task_lists = TaskList.objects.filter(user=request.user)
        for task_list in task_lists:
            # print(task_list)
            task_list_dict[task_list] = task_list.task_set.all()
        # selected_tasks = TaskList.objects.prefetch_related('user').all()
        context = {'object_list':tasks, 'task_lists':task_lists, 'task_list_dict':task_list_dict}
        return render(request, 'todoApp/home_view.html', context=context)
    return render(request, 'accounts/login.html')

@login_required
def search_view(request):
    context={}
    tasks = []
    task_list_dict = {}
    task_lists = TaskList.objects.filter(user=request.user)
    for task_list in task_lists:
        task_list_dict[task_list] = task_list.task_set.all()
        for task in task_list.task_set.all():
            tasks.append(task)
    if request.method == 'POST':
        task_title = request.POST['task_title']
        # task_title = query_dict.get('task_title')
        task_object = None
        # print(request.user.is_authenticated, request.user)
        # TODO: zadania pokazuja sie tylko danego usera ZROBIONE 08.01
        if task_title is not None:
            try:
                #TODO: Mozesz szukac po slowniku 16.01
                #TODO: Ma szukac również TaskList do ktorych mozna przejsc zeby zobaczyc inne taski z tego TaskList 16.01
                # task_object = Task.objects.get(title=task_title, user=request.user)
                task_object = [task for task in tasks if task_title in str(task)]
            except:
                return render(request, 'todoApp/task_search.html')
        speicfic_task_list = TaskList.objects.filter(user=request.user)
        context = {'task_object':task_object, 'tasks':tasks, 'task_lists': task_lists, 'task_title': task_title, 'task_list_dict': task_list_dict}
        return render(request, 'todoApp/task_search.html', context=context)
    return render(request, 'todoApp/task_search.html', context=context)

@login_required
def create_view(request):
    context={}
    task_form = TaskForm(request.POST or None)
    # print('2', request.session.get('moj_plik'))
    if task_form.is_valid():
        task_list_id = TaskList.objects.filter(title=request.session.get('moj_plik')).first().id
        title = task_form.cleaned_data.get("title")
        description = task_form.cleaned_data.get("description")
        # task_list_object = TaskList.objects.create(title=task_list, user=request.user)
        Task.objects.create(task_list_id=task_list_id, title=title, description=description)
        tasks = TaskList.objects.filter(user=request.user)
        context = {'tasks':tasks, 'description': description, 'form': task_form, 'created':True}
        return render(request, 'todoApp/specific_tasklist.html', context=context)
    tasks = TaskList.objects.filter(user=request.user)
    context = {'tasks':tasks, 'form': task_form}
    return render(request, 'todoApp/task_create.html', context=context)

@login_required
def create_task_list_view(request):
    context={}
    task_list_form = TaskListForm(request.POST or None)
    if task_list_form.is_valid():
        title = task_list_form.cleaned_data.get("title")
        user = request.user
        TaskList.objects.create(title=title, user=user)
        task_list_id = TaskList.objects.filter(title=title).first().id
        context = {'form': task_list_form}
        return redirect(f'/tasklist/{task_list_id}')
    context = {'form': task_list_form}
    return render(request, 'todoApp/task_list_create.html', context=context)

# @login_required
# def task_create_view(request):
#     context={}
#     task_form = CreateTaskForm(request.POST or None)
#     if task_form.is_valid():
#         # task_list = request.POST.get('task_list')
#         title = request.POST['task_title']
#         # description = task_form.cleaned_data.get("description")
#         # task_list_object = TaskList.objects.create(title=task_list, user=request.user)
#         Task.objects.create(task_list=task_list_object, title=title, description=description)
#         tasks = TaskList.objects.filter(user=request.user)
#         context = {'tasks':tasks, 'description': description, 'form': task_form, 'created':True}
#         return render(request, 'todoApp/task_create.html', context=context)
#     tasks = TaskList.objects.filter(user=request.user)
#     context = {'tasks':tasks, 'form': task_form}
#     return render(request, 'todoApp/task_create.html', context=context)


@login_required
def delete_view(request, id):
    # delete_form = TaskFormDelete(request.POST or None)
    # if task_form.is_valid():
    #     title = task_form.cleaned_data.get('title')
    #     Task.objects.filter(title=title).delete()
    #     context = {'form': task_form, 'deleted': True}
    #     return render(request, 'todoApp/task_delete.html', context=context)
    # return render(request, 'todoApp/task_delete.html', context={'form': task_form})
    # if delete_form.is_valid():
    # if request.method == 'POST':
        # id = request.POST.get('id')
    # task_list = TaskList.objects.filter(id=id).first()
    # tasks = task_list.task_set.all()
    request.session.get('moj_plik')
    task_list_id = TaskList.objects.filter(title=request.session.get('moj_plik')).first().id
    Task.objects.filter(id=id).delete()
    return redirect(f'/tasklist/{task_list_id}')

def complete_view(request, id):
    request.session.get('moj_plik')
    task_list_id = TaskList.objects.filter(title=request.session.get('moj_plik')).first().id
    task = Task.objects.filter(id=id).first()
    if task.complete is True:
        Task.objects.filter(id=id).update(complete=False)
        return redirect(f'/tasklist/{task_list_id}')
    else:
        Task.objects.filter(id=id).update(complete=True)
        return redirect(f'/tasklist/{task_list_id}')

#TODO: zrobic widok ktory przenosi nas do detali task view na podstawie pk ZROBIONE: 06.02
#TODO: Możliwość usunięcia oraz edycji taska przyciskiem obok 
#TODO: Mozliwość oznaczenia taska jako zrobionego
#TODO: zmienna sesyjna
def specific_tasklist_view(request, id):
    task_form = CreateTaskForm(request.POST or None)
    if task_form.is_valid():
        task_list_id = TaskList.objects.filter(title=request.session.get('moj_plik')).first().id
        title = task_form.cleaned_data.get("title")
        # description = task_form.cleaned_data.get("description")
        Task.objects.create(task_list_id=task_list_id, title=title, description='')
        tasks = TaskList.objects.filter(user=request.user)
        context = {'tasks':tasks, 'form': task_form, 'created':True}
        print("SRODEK CREATE")
        return redirect(f'/tasklist/{task_list_id}')
    task_list = TaskList.objects.filter(id=id).first()
    request.session['moj_plik'] = str(task_list)
    # print(request.session.get('moj_plik'))
    tasks = task_list.task_set.all()
    context = {'tasks':tasks, 'task_list':task_list, 'form':task_form}
    return render(request, 'todoApp/specific_tasklist.html', context=context)
