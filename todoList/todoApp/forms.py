from django import forms
from .models import Task, TaskList

class TaskListForm(forms.ModelForm):
    class Meta:
        model = TaskList
        fields = ['title']
        
        widgets = {
        'title': forms.TextInput(attrs={'class': 'form-control m-2', 'placeholder': 'Title'}),
        }
        
        labels = {
            "title": ""
        }

class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['title', 'description']

    def clean(self):
        cleaned_data = self.cleaned_data
        tasks = Task.objects.all()
        title = cleaned_data.get('title')
        if Task.objects.filter(title=title).exists():
            self.add_error('title', 'This task is taken.')
        return cleaned_data

class TaskFormDelete(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['task_list', 'title']

class CreateTaskForm(forms.ModelForm):
    # title = forms.CharField(widget=forms.TextInput, label='')
    class Meta:
        model = Task
        fields = ['title']

        widgets = {
        'title': forms.TextInput(attrs={'class': 'form-control m-2', 'placeholder': 'Title'}),
        }
        
        labels = {
            "title": ""
        }
    