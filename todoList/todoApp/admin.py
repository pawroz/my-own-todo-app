from django.contrib import admin
from .models import Task, TaskList

admin.site.register(Task)

class TaskListAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'create_date')



admin.site.register(TaskList, TaskListAdmin)