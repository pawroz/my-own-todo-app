"""todoList URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from todoApp.views import (
    home_view,
    search_view,
    create_view,
    delete_view,
    complete_view,
    specific_tasklist_view,
    create_task_list_view
    )
from accounts.views import (
    login_view,
    logout_view,
    register_view,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_view, name='home'),
    path('tasks/', search_view, name='task-search'),
    path('create_task/', create_view, name='task-create'),
    path('create_task_list/', create_task_list_view, name='task-list-create'),
    path('delete_task/<int:id>', delete_view, name='task-delete'),
    path('complete_task/<int:id>', complete_view, name='task-complete'),
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('register/', register_view, name='register'),
    path('tasklist/<int:id>', specific_tasklist_view, name='specific_tasklist_view'),
    # path('check_tasks/', check_tasks_view, name='check_tasks'),
]
