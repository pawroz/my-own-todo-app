from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class CreateUserForm(UserCreationForm):
	# email = forms.EmailField(required=True)
#TODO: nowy registration form z poprawionymi widgetami 31.01
	class Meta:
		model = User
		fields = ("username", "email", "password1")

		widgets = {
			'username': forms.TextInput(attrs={'class': 'form-control m-2', 'placeholder': 'Username'}),
			'email': forms.EmailInput(attrs={'class': 'form-control m-2', 'placeholder': 'Email'}),
		}
		
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.fields['password1'].widget.attrs.update({'class': 'form-control m-2',  'placeholder': 'Password'})
		self.fields['password2'].widget.attrs.update({'class': 'form-control m-2',  'placeholder': 'Reapet password'})

	# def save(self, commit=True):
	# 	user = super(NewUserForm, self).save(commit=False)
	# 	user.email = self.cleaned_data['email']
	# 	if commit:
	# 		user.save()
	# 	return user

